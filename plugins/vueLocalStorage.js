import Vue from 'vue'
import chartjs from 'vue-chartjs'
import vueChartist from 'vue-chartist'
import VueLocalStorage from 'vue-localstorage'
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueLocalStorage, {
  name: 'ls',
  bind: true // created computed members from your variable declarations
})
Vue.use(chartjs)
Vue.use(vueChartist)
Vue.component('apexchart', VueApexCharts)
