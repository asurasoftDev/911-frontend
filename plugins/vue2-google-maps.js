import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCKjSGDr330PYbYzkFrJjAkE4CF2fbi38k',
    libraries: 'places'
  }
})
