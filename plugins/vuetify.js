import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'
import VuetifyConfirm from 'vuetify-confirm'
import '@mdi/font/css/materialdesignicons.css'
Vue.use(VuetifyConfirm)

// Vue.use(VuetifyConfirm, {
//   buttonTrueText: 'Accept',
//   buttonFalseText: 'Discard',
//   color: 'warning',
//   icon: 'warning',
//   title: 'Warning',
//   width: 350,
//   property: '$confirm'
// })

Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken2,
    accent: colors.cyan,
    secondary: colors.amber.darken3,
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: colors.deepOrange.accent4,
    success: colors.red.accent3
  },
  icons: {
    iconfont: 'mdi' // default - only for display purposes
  }
})
