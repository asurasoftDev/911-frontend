import AuthService from '../services/AuthService'
export default function({ redirect }) {
  const auth = new AuthService()
  auth.loadSesion()
  if (auth.token) {
    return redirect('/')
  }
}
