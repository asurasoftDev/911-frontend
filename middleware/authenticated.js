import AuthService from '../services/AuthService'
import { USER_INFO } from '../services/GLOBAL'
export default function({ redirect }) {
  const auth = new AuthService()
  auth.loadSesion()
  const user = USER_INFO.getValue()
  if (!user.auth) {
    return redirect('/login')
  }
}
