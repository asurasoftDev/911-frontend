import axios from 'axios-observable'
import { map } from 'rxjs/operators'
import { of } from 'rxjs'
import { URL_API, USER_INFO } from './GLOBAL'
export default class CRUDService {
  URL = URL_API

  constructor() {
    axios.interceptors.request.use(
      config => {
        const user = USER_INFO.getValue()
        // Do something before request is sent
        // config.headers.authorization = USER_INFO.getValue()
        config.headers.Accept = 'application/json'
        // console.log('user ', user)
        if (user && user.auth) {
          config.headers = { ...config.headers, ...user.auth }
        }

        // config.headers.Origin = 'http://localhost:3000'
        // console.log('ESTO ES INTERCEPTANDO', config)
        return config
      },
      function(error) {
        console.log('CRUDService=> ', error)
        // Do something with request error
        return Promise.reject(error)
      }
    )
  }

  httpRailsHelper(name, method, params) {
    if (method === 'get' || method === 'delete') {
      const miob = params ? '/' + params : ''
      return axios[method](`${this.URL}/api/v1/${name}${miob}`)
    } else if (method === 'put' || method === 'patch' || method === 'post') {
      return axios[method](`${this.URL}/api/v1/${name}`, params)
    } else {
      return of(Error())
    }
  }

  validateSession() {
    return axios.get(`${this.URL}/api/v1/auth/validate_token`).pipe(
      map(res => {
        console.log('RES validateSession', res.data)
        return res
      })
    )
  }

  getAllDias() {
    return axios.get(`${this.URL}/api/v1/dia`).pipe(
      map(res => {
        console.log('RES dias', res.data)
        return res
      })
    )
  }

  // PERSONAS
  getAllPersonas() {
    return axios.get(`${this.URL}/api/v1/personas`).pipe(
      map(res => {
        console.log('RES getAllPersonas', res.data)
        return res
      })
    )
  }

  updatePersona(data) {
    return axios.put(`${this.URL}/api/v1/personas`, data).pipe(
      map(res => {
        console.log('RES updatePersonas', res.data)
        return res
      })
    )
  }

  createPersona(data) {
    return axios.post(`${this.URL}/api/v1/personas`, data).pipe(
      map(res => {
        console.log('RES createPersona', res.data)
        return res
      })
    )
  }

  deletePersona(id) {
    return axios.delete(`${this.URL}/api/v1/personas?id=${id}`).pipe(
      map(res => {
        console.log('RES deletePersona', res.data)
        return res
      })
    )
  }

  // CIUDADES
  getAllCiudad() {
    return axios.get(`${this.URL}/api/v1/ciudades`).pipe(
      map(res => {
        console.log('RES getAllCiudad', res.data)
        return res
      })
    )
  }

  updateCiudad(data) {
    return axios.put(`${this.URL}/api/v1/ciudades`, data).pipe(
      map(res => {
        console.log('RES updateCiudad', res.data)
        return res
      })
    )
  }

  createCiudad(data) {
    return axios.post(`${this.URL}/api/v1/ciudades`, data).pipe(
      map(res => {
        console.log('RES createCiudad', res.data)
        return res
      })
    )
  }

  deleteCiudad(id) {
    return axios.delete(`${this.URL}/api/v1/ciudades?id=${id}`).pipe(
      map(res => {
        console.log('RES deleteCiudad', res.data)
        return res
      })
    )
  }

  // CARGOS
  getAllCargos() {
    return axios.get(`${this.URL}/api/v1/cargos`).pipe(
      map(res => {
        console.log('RES getAllcargos', res.data)
        return res
      })
    )
  }

  updateCargo(data) {
    return axios.put(`${this.URL}/api/v1/cargoss`, data).pipe(
      map(res => {
        console.log('RES updateCargo', res.data)
        return res
      })
    )
  }

  createCargo(data) {
    return axios.post(`${this.URL}/api/v1/cargoss`, data).pipe(
      map(res => {
        console.log('RES creatCargo', res.data)
        return res
      })
    )
  }

  deleteCargo(id) {
    return axios.delete(`${this.URL}/api/v1/cargoss?id=${id}`).pipe(
      map(res => {
        console.log('RES deletCargo', res.data)
        return res
      })
    )
  }
  // createUser(userData) {
  //   return axios.post(`${this.URL}/api/v1/user/signin`, userData).pipe(
  //     map(res => {
  //       console.log('RES createUser', res.data)
  //       return res
  //     })
  //   )
  // }
}
