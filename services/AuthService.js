import Vue from 'vue'
import axios from 'axios-observable'
import { map } from 'rxjs/operators'
import { URL_API, USER_INFO, CURRENT_USER } from './GLOBAL'
export default class AuthService {
  URL = URL_API

  PREFIX = '911-'

  token

  userInfo

  config

  localStorage

  constructor() {
    this.loadSesion()
  }

  login(email, password) {
    const instance = axios.create()
    return instance
      .post(`${this.URL}/api/v1/auth/sign_in`, { email, password })
      .pipe(
        map(res => {
          // ['access-token', 'expiry', 'token-type', 'uid', 'client']
          // console.log('res', res)
          const body = res.data
          if (body.success) {
            // console.log('HEADER::RESS:::', res.headers, body)
            const obj = {}
            obj['access-token'] = res.headers['access-token']
            obj.expiry = res.headers.expiry
            obj['token-type'] = res.headers['token-type']
            obj.uid = res.headers.uid
            obj.client = res.headers.client
            console.log('HEADER::OBJ:::', body)
            this.saveSesion({ auth: obj })
          }
          return res
        })
      )
  }

  logOut() {
    // const userInfo = USER_INFO.getValue()
    // const token = USER_TOKEN.getValue()
    // Vue.ls.remove(`${this.PREFIX}token`, token)
    Vue.ls.remove(`${this.PREFIX}user`)
    Vue.ls.remove(`accessToken`)
    Vue.ls.remove(`expiry`)
    Vue.ls.remove(`uid`)
    Vue.ls.remove(`tokenType`)
    Vue.ls.remove(`client`)
    USER_INFO.next(null)
    CURRENT_USER.next(null)
    window.location.reload()
    // USER_TOKEN.next(null)
  }

  setSesion(userInfo) {
    // console.log('setSesion', userInfo)
    // console.log('setSesion', userInfo)
    USER_INFO.next(userInfo)
  }

  saveSesion(userInfo) {
    this.setSesion(userInfo)
    Vue.ls.set(`${this.PREFIX}user`, JSON.stringify(userInfo))
  }

  loadSesion() {
    const user = Vue.ls.get(`${this.PREFIX}user`, undefined)
    // console.log('catgando sesion:: ', user)

    if (user && JSON.parse(user)) {
      // console.log('paso', user)
      this.setSesion(JSON.parse(user))
    }
  }
}
