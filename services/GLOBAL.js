import { BehaviorSubject } from 'rxjs'

// export const URL_API = 'http://911.asurasoft.net:3001'
// export const URL_API = 'http://192.168.0.116:1337'
// export const URL_API = 'http://192.168.0.110:1337'
// export const URL_API = 'http://10.0.0.32:3000'
export const URL_API = 'http://localhost:1337'
export const USER_INFO = new BehaviorSubject({})
export const CURRENT_USER = new BehaviorSubject({})
// export const USER_TOKEN = new BehaviorSubject({})
// export const USER_CONFIG = new BehaviorSubject({})

export const sacarText = xValue => {
  let text = ''
  // console.log(
  //   'dentro sacarText..',
  //   xValue,
  //   Object.prototype.toString.call(xValue)
  // )
  if (Object.prototype.toString.call(xValue) === '[object String]') {
    text += xValue + ' '
  } else if (Object.prototype.toString.call(xValue) === '[object Object]') {
    const aKeys = Object.keys(xValue)
    for (let i = 0; i < aKeys.length; i++) {
      const cKey = aKeys[i]
      if (cKey !== 'traces' && cKey !== 'status') {
        text += `${cKey} ${sacarText(xValue[cKey])}`
      }
    }
  } else if (Object.prototype.toString.call(xValue) === '[object Array]') {
    for (let i = 0; i < xValue.length; i++) {
      text += sacarText(xValue[i])
    }
  }
  return text
}
export const PERMIS = {
  PERM_VER_LIST: 1,
  PERM_VER: 2,
  PERM_CREAR: 3,
  PERM_ACTUALIZAR: 4,
  PERM_ELIMINAR: 5,

  'Edad Promedio por Ciudad': 14,
  'Emergencia Dia y noche': 15,
  'Empleados por Cargo': 16,
  'Promedio de Accidentado por ciudad': 18,
  'Insumos por emergencia': 17,
  'Cantidad de producto por emergencia': 19,
  'Segmentar por periodos emergencia': 20,
  'Productos mas consumidos': 21,
  'Ambulancias mas utilizadas': 22,
  'Productos mas consumidos por emergencia': 23,
  'Pacientes fallecidos por ciudad': 24
}

export const PERMISOS = {}
export const PERMITIDOS = {
  Calendario: 0,
  Cargos: 2,
  Compras: 1,
  Hospitales: 9,
  Personas: 3,
  Productos: 7,
  Reportes: 11,
  Roles: 10,
  Tandas: 4,
  Turnos: 5,
  'Turnos Vehiculos': 8,
  Vehiculos: 6,
  Emergencias: 12,
  'Mis Emergencias': 13
}

export const comprobarPermisos = () => {
  const permisos = CURRENT_USER.getValue().permisos
  console.log('permisos', JSON.stringify(permisos))
  // const pruena = {}
  for (let index = 0; index < permisos.length; index++) {
    const permiso = permisos[index]
    const nombre = PERMITIDOS[permiso.nombre]
    if (nombre !== undefined) {
      if (PERMISOS[nombre] === undefined) {
        PERMISOS[nombre] = []
      }
      // console.log('permiso.accion.nombre', permiso)
      permiso.acciones.forEach(accion => {
        if (accion.nombre.indexOf('Ver listado') > -1) {
          PERMISOS[nombre].push(PERMIS.PERM_VER_LIST)
        } else if (accion.nombre.indexOf('Ver') > -1) {
          PERMISOS[nombre].push(PERMIS.PERM_VER)
        } else if (
          accion.nombre.indexOf('Crear') > -1 ||
          accion.nombre.indexOf('Asignar') > -1
        ) {
          // console.log('Compras ', accion.nombre, 'nombre', nombre)
          PERMISOS[nombre].push(PERMIS.PERM_CREAR)
        } else if (accion.nombre.indexOf('Actualizar') > -1) {
          PERMISOS[nombre].push(PERMIS.PERM_ACTUALIZAR)
        } else if (accion.nombre.indexOf('Eliminar') > -1) {
          PERMISOS[nombre].push(PERMIS.PERM_ELIMINAR)
        } else if (permiso.nombre === 'Reportes') {
          if (accion.nombre === 'Edad Promedio por Ciudad') {
            PERMISOS[nombre].push(PERMIS['Edad Promedio por Ciudad'])
          } else if (accion.nombre === 'Emergencia Dia y noche') {
            PERMISOS[nombre].push(PERMIS['Emergencia Dia y noche'])
          } else if (accion.nombre === 'Empleados por Cargo') {
            PERMISOS[nombre].push(PERMIS['Empleados por Cargo'])
          } else if (accion.nombre === 'Promedio de Accidentado por ciudad') {
            PERMISOS[nombre].push(PERMIS['Promedio de Accidentado por ciudad'])
          } else if (accion.nombre === 'Insumos por emergencia') {
            PERMISOS[nombre].push(PERMIS['Insumos por emergencia'])
          } else if (accion.nombre === 'Cantidad de producto por emergencia') {
            PERMISOS[nombre].push(PERMIS['Cantidad de producto por emergencia'])
          } else if (accion.nombre === 'Segmentar por periodos emergencia') {
            PERMISOS[nombre].push(PERMIS['Segmentar por periodos emergencia'])
          } else if (accion.nombre === 'Segmentar por periodos emergencia') {
            PERMISOS[nombre].push(PERMIS['Segmentar por periodos emergencia'])
          } else if (accion.nombre === 'Productos mas consumidos') {
            PERMISOS[nombre].push(PERMIS['Productos mas consumidos'])
          } else if (accion.nombre === 'Ambulancias mas utilizadas') {
            PERMISOS[nombre].push(PERMIS['Ambulancias mas utilizadas'])
          } else if (
            accion.nombre === 'Productos mas consumidos por emergencia'
          ) {
            PERMISOS[nombre].push(
              PERMIS['Productos mas consumidos por emergencia']
            )
          } else if (accion.nombre === 'Pacientes fallecidos por ciudad') {
            PERMISOS[nombre].push(PERMIS['Pacientes fallecidos por ciudad'])
          }
        }
      })
    }
  }
  console.log('PERMISOS', PERMISOS)
  // console.log('pruena', pruena)
}

export const tengoPermiso = (permiso, PermisAction) => {
  let rep = false
  if (PERMISOS[permiso]) {
    rep = PERMISOS[permiso].indexOf(PermisAction) > -1
  }
  // console.log('tengoPermiso=> rep ', rep)
  return rep
}
